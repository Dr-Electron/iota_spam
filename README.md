# IOTA_SPAM

## Compilation on Windows: (This README assumes that you are using a 64 bit system)

1. Install *bazel* according to this [manual](https://docs.bazel.build/versions/master/install-windows.html).
2. Install *MYSYS* according to this [manual](https://www.msys2.org/).
3. Install *mingw-gcc*. Open a MSYS shell and type `pacman -S mingw-w64-x86_64-gcc`.
4. Install Git tools. One solution (there are many and I cannot rate them) can be found [here](https://gitforwindows.org/).
5. Open a shell of your choice (PS,CMD,Git CMD) and clone this repository: `git clone https://gitlab.com/herrkpunkt/iota_spam.git`
6. Compile the binaries with: `bazel.exe build --compiler=mingw-gcc //app:spam_blowball`, `bazel.exe build --compiler=mingw-gcc //app:spam_double_spend` and `bazel.exe build --compiler=mingw-gcc //app:spam_healthy`
